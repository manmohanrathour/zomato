import Statistics from './Statistics';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCollections } from '../actions/FetchCollections';
import { Link } from 'react-router-dom';
import Homepage from './HomePage';
import { fetchCollectionsDes } from '../actions/FetchCollectionDes';

const styles = {
  width: '60%',
  marginLeft: '10vw',
  backgroundColor: 'transparent'
};
const stylesLink = {
  backgroundColor: 'white',
  height: '3vw',
  textAlign: 'center',
  color: '#CB202D',
  padding: '10px',
  fontSize: '14px',
  borderRadius: '5px',
  cursor: 'pointer'
};
class CollectionsCards extends Component {
  constructor() {
    super();
    this.state = {
      city: ''
    };
  }
  componentDidMount() {
    this.setState({
      city: this.props.city
    });

    this.props.fetchCollections(this.state.city);
  }
  handleAddCollection = id => {
    this.props.fetchCollectionsDes(id, this.props.city);
  };

  render() {
    const sliceArray =
      this.props.collections.data &&
      this.props.collections.data.collections.slice(0, 4);

    const dispalyCards =
      this.props.collections.data &&
      sliceArray.map(d => {
        return (
          <div
            className="col-md-6 "
            key={d.collection.collection_id}
            onClick={() => this.handleAddCollection(d.collection.collection_id)}
          >
            <div className="card">
              <div className="row">
                <div className="col-sm-4">
                  <img
                    className="br32"
                    src={d.collection.image_url}
                    alt="Find the best restaurants, cafés, and bars in Bengaluru"
                  />
                </div>
                <div className="col-sm-8">
                  <Link
                    to={`/bangalore/collection/${d.collection.title}`}
                    style={{ color: 'inherit' }}
                  >
                    <h6 className="font-weight-bold ml-4 mt-2 ">
                      {d.collection.title}
                    </h6>
                    <p
                      className="description ml-4"
                      style={{
                        lineHeight: 1.2,
                        fontSize: '14px',
                        width: '80%'
                      }}
                    >
                      {d.collection.description}
                    </p>
                  </Link>
                </div>
              </div>
            </div>
            <br />
          </div>
        );
      });

    return (
      <React.Fragment>
        <Homepage />
        <div className="colections" style={styles}>
          <a href="https://www.zomato.com/zomaland">
            <div className="AdsBanner"></div>
          </a>
          <div>
            <h4 style={{ fontWeight: 'bold', marginTop: '2vw' }}>
              Collections
            </h4>
            <p
              style={{
                color: '#89959B',
                fontWeight: '16px',
                marginTop: '-0.5vw'
              }}
            >
              Explore curated lists of top restaurants, cafes, pubs, and bars in
              Bengaluru, based on trends
            </p>
          </div>
          <div className="row">{dispalyCards}</div>
          <div className="row" id="x">
            <Link to="/bangalore/collections">
              <div className="col-md-12" style={stylesLink}>
                All collections in Bengaluru
              </div>
            </Link>
          </div>
          <Statistics />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  collections: state.collections.data,
  city: state.collections.city
});

export default connect(mapStateToProps, {
  fetchCollections,
  fetchCollectionsDes
})(CollectionsCards);
