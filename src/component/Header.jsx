import React from 'react';
import SearchBar from './SearchBar';
import { fetchCollections } from '../actions/FetchCollections';
import { fetchCuisines } from '../actions/FetchCuisines';
import { connect } from 'react-redux';

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      location: '',
      cuisines: ''
    };
  }

  getInputValue = (e, key) => {
    var state = {};
    state[key] = e;
    this.setState(state);
  };

  handleSearch = () => {
    this.props.fetchCuisines(this.state.location, this.state.cuisines,'','');
  };

  render() {
    return (
      <div className="zomato-header navbar" id="header">
        <div className="zomato-header-wrapper clearfix">
          <a className="logo__container" href="/banglore">
            <img
              src="https://b.zmtcdn.com/images/zomato_white_logo_new.svg"
              alt="Restaurants in India"
            ></img>
          </a>
        
          <SearchBar
            getInputValue={this.getInputValue}
            handleSearch={this.handleSearch}
          />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  collections: state.collections.data
});
export default connect(mapStateToProps, {
  fetchCollections,
  fetchCuisines
})(Header);
