import React from 'react';
import { fetchCuisines } from '../actions/FetchCuisines';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const styles = {
  background: '#cb202d',
  width: '7vw',
  height: '100%',
  borderRadius: '5px',
  textAlign: 'center',
  color: 'white',
  border: 'transparent'
};

class HomePage extends React.Component {
  constructor() {
    super();
    this.state = {
      location: '',
      cuisines: ''
    };
  }

  getInputValue = (e, key) => {
    var state = {};
    state[key] = e;
    this.setState(state);
  };

  handleSearch = () => {
    this.props.fetchCuisines(this.state.location, this.state.cuisines);
  };
  render() {
    return (
      <div className="homeImage">
        <a
          className="logo--header"
          href="/bangalore"
          title="Find the best restaurants, cafés, and bars in Bengaluru"
        >
          <img
            className="br3"
            src="https://b.zmtcdn.com/images/logo/zomato_flat_bg_logo.svg"
            alt="Find the best restaurants, cafés, and bars in Bengaluru"
          />
        </a>
        <p
          className="h-city-home-title"
          style={{ color: 'white', fontSize: '25px' }}
        >
          Find the best restaurants, cafés, and bars in Bengaluru{' '}
        </p>
        <div>
          <div
            className="search_bar"
            id="search_bar_wrapper"
            style={{ marginTop: '1vw' }}
          >
            <div className="search_location mr-2">
              <i className="fa fa-search"></i>
              <input
                className="location"
                id="location_select"
                type="text"
                autoComplete="off"
                placeholder="Bengaluru"
                aria-expanded="true"
                style={{ fontWeight: 'bold', height: '3vw' }}
                onChange={e => this.getInputValue(e.target.value, 'location')}
              />
              <i className="fa fa-caret-down"></i>
            </div>

            <div className="search_box mr-2">
              <i className="fa fa-search"></i>

              <input
                className="location"
                type="text"
                autoComplete="off"
                placeholder="Search for restaurants or cuisines..."
                aria-expanded="true"
                style={{ height: '3vw' }}
                onChange={e => this.getInputValue(e.target.value, 'cuisines')}
              />
            </div>
            <div className="mmn plr0i">
              <Link to={`/${this.state.location}/collections/cuisines`} style={{ color: 'inherit' }}>
                <button
                  role="button"
                  tabIndex="0"
                  id="search_button"
                  style={styles}
                  onClick={() => this.handleSearch()}
                >
                  Search
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null,{
  fetchCuisines
})(HomePage);
