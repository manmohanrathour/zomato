import React from 'react';
import { Link } from 'react-router-dom';

const cardComponent = props => {
  return (
    <div
      className="col-md-4"
      key={props.data.collection.collection_id}
      id={props.data.collection.collection_id}
      onClick={() =>
        props.addCollectionsDes(props.data.collection.collection_id)
      }
      style={{ cursor: 'pointer' }}
    >
      <div className="card">
        <div className="row">
          <div className="col-sm-4">
            <img
              className="br32"
              src={props.data.collection.image_url}
              alt="Find the best restaurants, cafés, and bars in Bengaluru"
            />
          </div>
          <div className="col-sm-8">
            <Link
              to={`/bangalore/collection/${props.data.collection.title}`}
              style={{ color: 'inherit' }}
            >
              <h6 className="font-weight-bold ml-5 mt-2">
                {props.data.collection.title}
              </h6>
              <p
                className="description ml-5"
                id="mm"
                style={{
                  lineHeight: 1.2,
                  fontSize: '14px',
                  width: '80%',
                  height: '2.5vw',
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  whiteSpace: 'nowrap',
                  
                }}
              >
                {props.data.collection.description}
              </p>
            </Link>
          </div>
        </div>
      </div>
      <br />
    </div>
  );
};
export default cardComponent;
