import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRestaurant } from '../actions/FetchRestaurant';
import Header from './Header';
const styles = {
  width: '120vh',
  height: '53vh',
  backgroundColor: 'white',
  borderRadius: '4px',
  marginLeft: '10vw'
};

class Restaurant extends Component {
  render() {
    console.log(this.props.restaurant.data && this.props.restaurant.data);
    return (
      <React.Fragment>
        <Header />
        <div style={styles}></div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  restaurant: state.restaurant.restaurant
});

export default connect(mapStateToProps, { fetchRestaurant })(Restaurant);
