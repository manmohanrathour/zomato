import React from 'react';

const Header = () => {
  return (
    <div className="ui statistics grey-text small margin0 d-flex justify-content-between">
      <div className="statistic">
        <div className="value font">24</div>
        <div className="label">COUNTRIES</div>
      </div>
      <div className="statistic">
        <div className="value">1.5M</div>
        <div className="label">RESTAURANTS</div>
      </div>
      <div className="statistic">
        <div className="value">120M</div>
        <div className="label">FOODIES EVERY MONTH</div>
      </div>
      <div className="statistic">
        <div className="value">30M</div>
        <div className="label">PHOTOS</div>
      </div>
      <div className="statistic">
        <div className="value">10M</div>
        <div className="label">REVIEWS</div>
      </div>
      <div className="statistic">
        <div className="value">18M</div>
        <div className="label">BOOKMARKS</div>
      </div>
    </div>
  );
};
export default Header;
