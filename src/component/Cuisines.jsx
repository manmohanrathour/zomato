import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCuisines } from '../actions/FetchCuisines';
import { fetchRestaurant } from '../actions/FetchRestaurant';
import { Link } from 'react-router-dom';
import Header from './Header';

const styles = {
  width: '100vh',
  height: '53vh',
  backgroundColor: 'white',
  borderRadius: '4px'
};
const ConatainerRest = {
  width: '95vh',
  marginLeft: '13px',
  padding: '12px',
  display: 'flex',
  justifyContent: 'space-between'
};
class Cuisines extends Component {
  handleSearch = (location, cuisines, type, order) => {
    this.props.fetchCuisines(location, cuisines, type, order);
  };
  searchRes = id => {
    this.props.fetchRestaurant(id);
  };

  render() {
    const data =
      this.props.cuisines.data && this.props.cuisines.data.restaurants;

    const restDetail =
      this.props.cuisines.data &&
      this.props.cuisines.data.restaurants &&
      this.props.cuisines.data.restaurants.map(d => {
        return (
          <React.Fragment>
            <div style={styles}>
              <div className="rest_nameConatainer" style={ConatainerRest}>
                <img
                  src={d.restaurant.featured_image}
                  alt="Restaurants in India"
                  style={{ width: '18vh', borderRadius: '6px', height: '9vw' }}
                ></img>

                <div className="detailsContainer">
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      width: '20vw'
                      // alignContent: 'center'
                    }}
                  >
                    <a
                      style={{
                        fontSize: 11,
                        color: '#89959B',
                        textTransform: 'uppercase'
                      }}
                    >
                      {d.restaurant.establishment[0]}
                    </a>
                    <Link
                      to={`/cuisines/restaurant/`}
                      style={{ color: 'inherit' }}
                    >
                      <a
                        style={{
                          color: '#cb202d',
                          fontSize: 24,
                          fontWeight: 'bold',
                          cursor: 'pointer'
                        }}
                        onClick={() => this.searchRes(d.restaurant.id)}
                      >
                        {d.restaurant.name}
                      </a>
                    </Link>
                    <b>{d.restaurant.location.locality}</b>
                    <div style={{ fontSize: 14, color: '#89959B' }}>
                      {d.restaurant.location.address}
                    </div>
                  </div>

                  <div>
                    <div
                      className="ratings"
                      style={{
                        background: `#${d.restaurant.user_rating.rating_color}`
                      }}
                    >
                      {d.restaurant.user_rating.aggregate_rating}
                    </div>
                    <div style={{ fontSize: 12, color: `#89959B` }}>
                      {d.restaurant.user_rating.votes} votes
                    </div>
                  </div>
                </div>
              </div>
              <hr style={{ width: '92vh', marginLeft: '32px' }} />
              <div
                style={{ width: '92vh', display: 'flex', marginLeft: '30px' }}
              >
                <div className="cuisionType">CUISINES:</div>
                <div className="addressType">{d.restaurant.cuisines}</div>
              </div>
              <div
                style={{ width: '92vh', display: 'flex', marginLeft: '30px' }}
              >
                <div className="cuisionType">COST FOR TWO:</div>
                <div className="addressType">
                  ₹{d.restaurant.average_cost_for_two}
                </div>
              </div>
              <div
                style={{ width: '92vh', display: 'flex', marginLeft: '30px' }}
              >
                <div className="cuisionType">HOURS:</div>
                <div className="addressType">
                  {d.restaurant.timings} (Mon-Sun)
                </div>
              </div>
              <div
                style={{ width: '92vh', display: 'flex', marginLeft: '30px' }}
              >
                <div className="cuisionType">FEATURED IN:</div>
                <div className="addressType">
                  Regional Favorites, Brilliant Biryanis
                </div>
              </div>
            </div>
            <br />
          </React.Fragment>
        );
      });
    return (
      <React.Fragment>
        <Header />

        <div
          style={{
            display: 'flex',
            marginLeft: '5vw',
            justifyContent: 'space-between',
            width: '63%'
          }}
        >
          <div
            style={{
              backgroundColor: 'white',
              width: '15vw',
              height: '100%',
              borderRadius: '4px',
              paddingLeft: '11px'
            }}
          >
            <div style={{ fontSize: '15px', fontWeight: 'bold' }}>Sort By</div>
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'biryani',
                  'rating',
                  'desc'
                );
              }}
            >
              Rating - high to low
            </a>
            <br />
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'biryani',
                  'cost',
                  'desc'
                );
              }}
            >
              Cost - high to low
            </a>
            <br />
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'biryani',
                  'cost',
                  'asc'
                );
              }}
            >
              Cost - low to high
            </a>
            <br/>
            <br/>
            <div style={{ fontSize: '15px', fontWeight: 'bold' }}>Cuisine</div>
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'Chinese',
                  '',
                  ''
                );
              }}
            >
              Chinese
            </a>
            <br />
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              id="Fast Food"
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'Fast Food',
                  '',
                  ''
                );
              }}
            >
              Fast Food
            </a>
            <br />
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              id="Bakery"
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'Bakery',
                  '',
                  ''
                );
              }}
            >
              Bakery
            </a>
            <br />
            <a
              style={{ fontSize: 14, cursor: 'pointer' }}
              id="Desserts"
              onClick={() => {
                this.handleSearch(
                  data[0].restaurant.location.city,
                  'Desserts',
                  '',
                  ''
                );
              }}
            >
              Desserts
            </a>
            <br />
          </div>
          <div>{restDetail}</div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cuisines: state.cuisines.cuisines
});

export default connect(mapStateToProps, { fetchCuisines, fetchRestaurant })(
  Cuisines
);
