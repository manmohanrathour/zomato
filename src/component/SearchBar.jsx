import React from 'react';
import { Link } from 'react-router-dom';

const SearchBar = props => {
  return (
    <div className="search_bar" id="search_bar_wrapper">
      <div className="search_location mr-2">
        <i className="fa fa-search"></i>
        <input
          className="location"
          id="location_select"
          type="text"
          autoComplete="off"
          placeholder="Bengaluru"
          aria-expanded="true"
          style={{ fontWeight: 'bold' }}
          onChange={e => props.getInputValue(e.target.value, 'location')}
        />
        <i className="fa fa-caret-down"></i>
      </div>

      <div className="search_box mr-2">
        <i className="fa fa-search"></i>

        <input
          className="location"
          type="text"
          autoComplete="off"
          placeholder="Search for restaurants or cuisines..."
          aria-expanded="true"
          onChange={e => {props.getInputValue(e.target.value, 'cuisines')}}
          
        />
      </div>
      <div className="mmn plr0i">
        <Link
          to={`/delhi/collections/cuisines`}
          style={{ color: 'inherit' }}
        >
          <div
            role="button"
            tabIndex="0"
            id="search_button"
            onClick={() => props.handleSearch()}
          >
            Search
          </div>
        </Link>
      </div>
    </div>
  );
};

export default SearchBar;
