import React, { Component } from 'react';
import { connect } from 'react-redux';
import CardComponent from './CardComponent';
import { fetchCollectionsDes } from '../actions/FetchCollectionDes';
import Header from './Header';
const styles1 = {
  marginTop: '3vw',
  width: '82.5vw',
  marginLeft: '10vw',

  backgroundColor: 'white',
  height: '31.5vw',
  borderRadius: '2px'
};
const styles = {
  width: '85vw',
  marginTop: '2vw',
  marginLeft: '9vw',
  // paddingRight: '6vw',
  backgroundColor: 'transparent'
};
const styles2 = {
  marginTop: '3vw',
  width: '85vw',
  marginLeft: '9vw',
  // display: 'inline',
  backgroundColor: 'transparent',
  height: '100%',
  borderRadius: '2px'
};

class CollectionsDes extends Component {
  handleAdd = id => {
    this.props.fetchCollectionsDes(id, this.props.city);
  };
  render() {
    const newArray =
      this.props.CollectionsDes.data &&
      this.props.coll.data.collections.filter(
        c => c.collection.collection_id === this.props.id
      );
    const collectionExceptCurrent =
      this.props.CollectionsDes.data &&
      this.props.coll.data.collections.filter(
        c => c.collection.collection_id !== this.props.id
      );

    const title = newArray && newArray[0].collection.title;
    const description = newArray && newArray[0].collection.description;
    const results_found =
      this.props.CollectionsDes.data &&
      this.props.CollectionsDes.data.results_found;
    const dispalyCards =
      this.props.CollectionsDes.data &&
      this.props.CollectionsDes.data.restaurants.map(d => {
        return (
          <div className="col-md-4">
            <div className="card" style={{ height: '18vw' }}>
              <div
                style={{
                  background: `url(${d.restaurant.featured_image})`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center center',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  borderRadius: '3px',
                  height: '10vw'
                }}
              >
                <div
                  className="rating"
                  style={{
                    background: `#${d.restaurant.user_rating.rating_color}`
                  }}
                >
                  {d.restaurant.user_rating.aggregate_rating}
                </div>
              </div>

              <p
                class="rest_name"
                style={{
                  fontSize: '17px',
                  fontWeight: 'bold',
                  marginTop: '1vw',
                  marginLeft: '1vw',
                  color: '#33373d'
                }}
              >
                {d.restaurant.name}
              </p>

              <p
                style={{
                  color: '#89959B',
                  fontSize: '12px',
                  marginLeft: '1vw',
                  marginTop: '-1vw',
                  marginBottom: '0.5vw',
                  textTransform: 'uppercase'
                }}
              >
                {d.restaurant.location.locality},{d.restaurant.location.city}
              </p>

              <p
                style={{
                  color: '#89959B',
                  fontSize: '14px',
                  marginLeft: '1vw',
                  marginTop: '-0.5vw',
                  marginBottom: '0.5vw'
                }}
              >
                {d.restaurant.cuisines}
              </p>
            </div>
            <br />
          </div>
        );
      });
    const collectionCards =
      collectionExceptCurrent &&
      collectionExceptCurrent.map(d => {
        return <CardComponent data={d} addCollectionsDes={this.handleAdd} />;
      });
    return (
      <React.Fragment>
        <Header />
        <div style={styles1}>
          <div className="Coll_image"></div>
          <h1
            style={{
              fontSize: '28px',
              fontWeight: 'bold',
              marginTop: '1vw',
              marginLeft: '1vw',
              color: '#33373d'
            }}
          >
            {title}
          </h1>
          <p
            style={{
              fontSize: '15px',
              marginTop: '-0.5vw',
              marginLeft: '1vw',
              color: '#33373d'
            }}
          >
            {description}
          </p>
          <p
            style={{
              color: '#89959B',
              fontSize: '14px',
              marginLeft: '1vw',
              marginTop: '-1vw',
              marginBottom: '0.5vw'
            }}
          >
            {results_found} Places
          </p>
          <button className="coll_button">Save Collection</button>
        </div>
        <div className="row" style={styles2}>
          {dispalyCards}
        </div>
        <div
          style={{
            marginTop: '3vw',
            width: '85vw',
            marginLeft: '10vw',
            backgroundColor: 'tranparent',
            borderRadius: '2px'
          }}
        >
          <h2 style={{ fontWeight: 'bold', fontSize: '18px' }}>
            More Collections
          </h2>
        </div>
        <div className="row" style={styles}>
          {collectionCards}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  CollectionsDes: state.collections.collDes,
  coll: state.collections.data,
  id: state.collections.collectionID,
  city: state.collections.city
});

export default connect(mapStateToProps, { fetchCollectionsDes })(
  CollectionsDes
);
