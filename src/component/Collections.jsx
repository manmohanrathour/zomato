import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCollections } from '../actions/FetchCollections';
import { fetchCollectionsDes } from '../actions/FetchCollectionDes';
import CardComponent from './CardComponent';
import Header from './Header';
const styles = {
  width: '100%',
  marginTop: '3vw',
  paddingLeft: '6vw',
  paddingRight: '6vw',
  backgroundColor: 'transparent'
};

class Collections extends Component {
  componentDidMount() {
    this.props.fetchCollections(this.props.city);
  }

  handleAdd = id => {
    this.props.fetchCollectionsDes(id, this.props.city);
  };

  render() {
    const dispalyCards =
      this.props.collections.data &&
      this.props.collections.data.collections.map(d => {
        return <CardComponent data={d} addCollectionsDes={this.handleAdd} />;
      });
    return (
      <React.Fragment>
        <div>
          <Header />
        </div>
        <div className="row" style={styles}>
          {dispalyCards}
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  collections: state.collections.data,
  city: state.collections.city
});

export default connect(mapStateToProps, {
  fetchCollections,
  fetchCollectionsDes
})(Collections);
