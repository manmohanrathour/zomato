import React, { Component } from 'react';

import './App.css';
import CollectionsCards from './component/CollectionsCards';
import { Route, Switch, Redirect } from 'react-router-dom';
import Collections from './component/Collections';
import Footer from './component/Footer';
import CollectionsDes from './component/CollectionDes';

import Cuisines from './component/Cuisines';
import Restaurant from './component/Restaurant';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/:city/collections/:name" component={Cuisines} />
          <Route exact path="/bangalore/collections" component={Collections} />
          <Route exact path="/cuisines/restaurant/" component={Restaurant}/>
          <Route
            exact
            path="/bangalore/collection/:name"
            component={CollectionsDes}
          />
          <Redirect exact from="/" to="/bangalore" />
          <Route exact path="/:name" component={CollectionsCards} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default App;
