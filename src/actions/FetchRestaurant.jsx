import { FETCH_RESTAURANT } from './Types';
// const API = 'af1311863d32749989cfe01693a41523';
const API = 'a5c51a5c832b4983ee8ccbbdc7b0c4fb';
import axios from 'axios';

export const fetchRestaurant = ID => dispatch => {
  axios
    .request({
      method: 'GET',
      url: `https://developers.zomato.com/api/v2.1/restaurant?res_id=${ID}`,
      headers: {
        'Content-type': 'application/json',
        'user-key': API
      }
    })
    .then(res => {
        console.log(res,"dffdff")
      dispatch({
        type: FETCH_RESTAURANT,
        restaurantData: res
      });
    })
    .catch(err => console.log(err));
};
