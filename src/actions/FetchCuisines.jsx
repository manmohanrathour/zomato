import { FETCH_CUISINES } from './Types';
// const API = 'af1311863d32749989cfe01693a41523';
const API = 'a5c51a5c832b4983ee8ccbbdc7b0c4fb';
import axios from 'axios';

export const fetchCuisines = (cityNa, cuisines, type, order) => dispatch => {
  const cityName = cityNa === 'Bangalore' ? 'Bengaluru' : cityNa;
  axios
    .request({
      method: 'GET',
      url: `https://developers.zomato.com/api/v2.1/cities?q=${cityName}`,
      headers: {
        'Content-type': 'application/json',
        'user-key': API
      }
    })
    .then(city => {
      if (city.data.location_suggestions[0] === undefined) {
        alert('wrong city');
      } else {
        const url =
          type === '' && order === ''
            ? `https://developers.zomato.com/api/v2.1/search?entity_id=${city.data.location_suggestions[0].id}&entity_type=city&q=${cuisines}`
            : `https://developers.zomato.com/api/v2.1/search?entity_id=${city.data.location_suggestions[0].id}&entity_type=city&q=biryani&sort=${type}&order=${order}`;
        axios
          .request({
            method: 'GET',
            url: url,

            headers: {
              'Content-type': 'application/json',
              'user-key': API
            }
          })
          .then(cuision => {
            dispatch({
              type: FETCH_CUISINES,
              cuisinesData: cuision
            });
          })

          .catch(err => console.log(err));
      }
    });
};
