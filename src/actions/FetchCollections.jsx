import { FETCH_COLLECTIONS } from './Types';
// const API = 'af1311863d32749989cfe01693a41523';
const API = 'a5c51a5c832b4983ee8ccbbdc7b0c4fb';
import axios from 'axios';

export const fetchCollections = city => dispatch => {
  const cityName = city === '' ? 'Bengaluru' : city;

  axios
    .request({
      method: 'GET',
      url: `https://developers.zomato.com/api/v2.1/cities?q=${cityName}`,
      headers: {
        'Content-type': 'application/json',
        'user-key': API
      }
    })
    .then(city => {
      if (city.data.location_suggestions[0] === undefined) {
        alert('wrong city');
      } else {
        axios
          .request({
            method: 'GET',
            url: `https://developers.zomato.com/api/v2.1/collections?city_id=${city.data.location_suggestions[0].id}`,
            headers: {
              'Content-type': 'application/json',
              'user-key': API
            }
          })
          .then(collection => {
            dispatch({
              type: FETCH_COLLECTIONS,
              collectionsData: collection,
              city: cityName
            });
          })
          .catch(err => console.log(err));
      }
    });
};
