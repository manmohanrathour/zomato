import { FETCH_COLLECTIONSDES } from './Types';
// const API = 'af1311863d32749989cfe01693a41523';
const API = 'a5c51a5c832b4983ee8ccbbdc7b0c4fb';
import axios from 'axios';

export const fetchCollectionsDes = (id, cityName) => dispatch => {
  axios
    .request({
      method: 'GET',
      url: `https://developers.zomato.com/api/v2.1/cities?q=${cityName}`,
      headers: {
        'Content-type': 'application/json',
        'user-key': API
      }
    })
    .then(city => {
      if (city.data.location_suggestions[0] === undefined) {
        alert('wrong city');
      } else {
        axios
          .request({
            method: 'GET',
            url: `https://developers.zomato.com/api/v2.1/search?entity_id=${city.data.location_suggestions[0].id}&entity_type=city&collection_id=${id}`,
            headers: {
              'Content-type': 'application/json',
              'user-key': API
            }
          })
          .then(collectionDes => {
            dispatch({
              type: FETCH_COLLECTIONSDES,
              collectionsDesData: collectionDes,
              collectionID: id
            });
          })
          .catch(err => console.log(err));
      }
    });
};
