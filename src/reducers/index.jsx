import { combineReducers } from 'redux';
import fetching_data from './fetchig_Data';

export default combineReducers({
  collections: fetching_data,
  collectionsDes: fetching_data,
  cuisines: fetching_data,
  restaurant: fetching_data
});
