import { FETCH_COLLECTIONS, FETCH_COLLECTIONSDES ,FETCH_CUISINES,FETCH_RESTAURANT} from '../actions/Types';

const initialSatate = {
  data: [],
  collDes: [],
  collectionID: '',
  city:'',
  cuisines:[],
  restaurant:[]
};
export default function(state = initialSatate, action) {
  switch (action.type) {
    case FETCH_COLLECTIONS:
      return {
        ...state,
        data: action.collectionsData,
        city:action.city
      };
    case FETCH_COLLECTIONSDES:
      return {
        ...state,
        collDes: action.collectionsDesData,
        collectionID: action.collectionID,
        
      };
      case  FETCH_CUISINES:
      return{
        ...state,
        cuisines:action.cuisinesData
      }
      case FETCH_RESTAURANT:
        return{
          ...state,
        restaurant:action.restaurantData
        }
    default:
      return state;
  }
}
